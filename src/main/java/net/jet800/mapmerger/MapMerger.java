/*
 * Copyright 2017 Vladimir Frolkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.jet800.mapmerger;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utility class providing methods of merging multiple maps into one with specified merging function.
 *
 * @author Vladimir Frolkin {@literal <jet800@gmail.com>}
 * @implNote This class relies on Java8 Streams and does not guarantee of element order and particularly their merging order.
 */
public class MapMerger {

    private MapMerger() {
    }

    /**
     * Merges two maps into third one.
     * If given key is associated in only one map this association would be stored as-is in resulting map.
     * Otherwise, resulting value would be result of the given merging function.
     *
     * @param one   First map to merge
     * @param two   Second map to merge
     * @param merge Function to merge values sharing a key in two maps
     * @return merged map
     */
    public static <K, V> Map<K, V> merge(Map<K, V> one, Map<K, V> two, BiFunction<? super V, ? super V, ? extends V> merge) {
        return merge(Stream.of(one, two), merge);
    }

    /**
     * Merges a list of maps into new one.
     * If given key is associated in only one map this association would be stored as-is in resulting map.
     * Otherwise, resulting value would be result of the given merging function.
     * In case of null or empty list provided returns empty Map as {@link Collections#emptyMap()}
     *
     * @param maps  {@link java.util.List} of maps to merge
     * @param merge Function to merge values sharing a key in two maps
     * @return merged map
     */
    public static <K, V> Map<K, V> merge(List<Map<K, V>> maps, BiFunction<? super V, ? super V, ? extends V> merge) {
        if (Objects.isNull(maps) || maps.isEmpty()) {
            return Collections.emptyMap();
        }
        return merge(maps.stream(), merge);
    }

    /**
     * Merges a Stream of maps into new one.
     * If given key is associated in only one map this association would be stored as-is in resulting map.
     * Otherwise, resulting value would be result of the given merging function.
     * In case of null Stream provided returns empty Map as {@link Collections#emptyMap()}
     *
     * @param maps  {@link Stream} of maps to merge
     * @param merge Function to merge values sharing a key in two maps
     * @return merged map
     */
    public static <K, V> Map<K, V> merge(Stream<Map<K, V>> maps, BiFunction<? super V, ? super V, ? extends V> merge) {
        Objects.requireNonNull(merge, "Merge function could not be null");
        if (Objects.isNull(maps)) {
            return Collections.emptyMap();
        }
        return maps.flatMap((map) -> map.entrySet().stream()).parallel().collect(Collectors.toConcurrentMap(Map.Entry::getKey, Map.Entry::getValue, merge::apply));
    }
}
