package net.jet800.mapmerger;

import org.junit.jupiter.api.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MapMergerTest {

    Map<String, String> map1, map2, map3;

    @BeforeAll
    void setUp() {
        map1 = new HashMap<>();
        map1.put("key1", "val1");
        map1.put("key2", "val2");
        map1.put("key3", "val3");
        map1.put("key4", "val4");
        map1.put("key5", "val5");

        map2 = new HashMap<>();

        map1.put("key3", "val3");
        map1.put("key4", "val4");

        map2.put("key1", "val1.2");
        map2.put("key2", "val2.2");
        map2.put("key5", "val5.2");

        map2.put("key6", "val6");
        map2.put("key7", "val7");
        map2.put("key8", "val8");

        map3 = new HashMap<>();

        map3.put("key1", "val1.3");
        map3.put("key3", "val3.3");

        map3.put("key8", "val8.3");
        map3.put("key9", "val9");
    }

    @Test
    @DisplayName("Two maps")
    void merge() {
        Map<String, String[]> ethalon = new HashMap<>();
        ethalon.put("key1", new String[]{"val1", "val1.2"});
        ethalon.put("key2", new String[]{"val2", "val2.2"});
        ethalon.put("key3", new String[]{"val3"});
        ethalon.put("key4", new String[]{"val4"});
        ethalon.put("key5", new String[]{"val5", "val5.2"});
        ethalon.put("key6", new String[]{"val6"});
        ethalon.put("key7", new String[]{"val7"});
        ethalon.put("key8", new String[]{"val8"});

        Map<String, String> merged = MapMerger.merge(map1, map2, (a, b) -> a + ":" + b);
        merged.forEach((key, value) -> System.out.println(String.format("%s -> %s", key, value)));
        merged.forEach((key, value) -> {
            String[] values = value.split(":");
            Arrays.sort(values);
            assertArrayEquals(ethalon.get(key), values);
        });
    }

    @Test
    @DisplayName("Three maps via list")
    void merge1() {
        Map<String, String[]> ethalon = new HashMap<>();
        ethalon.put("key1", new String[]{"val1", "val1.2", "val1.3"});
        ethalon.put("key2", new String[]{"val2", "val2.2"});
        ethalon.put("key3", new String[]{"val3", "val3.3"});
        ethalon.put("key4", new String[]{"val4"});
        ethalon.put("key5", new String[]{"val5", "val5.2"});
        ethalon.put("key6", new String[]{"val6"});
        ethalon.put("key7", new String[]{"val7"});
        ethalon.put("key8", new String[]{"val8", "val8.3"});
        ethalon.put("key9", new String[]{"val9"});
        List<Map<String, String>> maps = new ArrayList<>();
        maps.add(map1);
        maps.add(map2);
        maps.add(map3);
        Map<String, String> merged = MapMerger.merge(maps, (a, b) -> a + ":" + b);
        merged.forEach((key, value) -> System.out.println(String.format("%s -> %s", key, value)));
        merged.forEach((key, value) -> {
            String[] values = value.split(":");
            Arrays.sort(values);
            assertArrayEquals(ethalon.get(key), values);
        });

    }

    @Test
    @DisplayName("Three maps via Stream")
    void merge2() {
        Map<String, String[]> ethalon = new HashMap<>();
        ethalon.put("key1", new String[]{"val1", "val1.2", "val1.3"});
        ethalon.put("key2", new String[]{"val2", "val2.2"});
        ethalon.put("key3", new String[]{"val3", "val3.3"});
        ethalon.put("key4", new String[]{"val4"});
        ethalon.put("key5", new String[]{"val5", "val5.2"});
        ethalon.put("key6", new String[]{"val6"});
        ethalon.put("key7", new String[]{"val7"});
        ethalon.put("key8", new String[]{"val8", "val8.3"});
        ethalon.put("key9", new String[]{"val9"});
        List<Map<String, String>> maps = new ArrayList<>();
        maps.add(map1);
        maps.add(map2);
        maps.add(map3);
        Map<String, String> merged = MapMerger.merge(maps.stream(), (a, b) -> a + ":" + b);
        merged.forEach((key, value) -> System.out.println(String.format("%s -> %s", key, value)));
        merged.forEach((key, value) -> {
            String[] values = value.split(":");
            Arrays.sort(values);
            assertArrayEquals(ethalon.get(key), values);
        });
    }


}