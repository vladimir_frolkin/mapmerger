# MapMerger

Simple class that allows merging arbitrary maps either presented as List or Stream.
Provides convenient method of merging exactly two maps.

Underlying implementation is based on Java8 Streams, uses parallel processing and does not guarantee any element order.

## Example usage

    Map<String, String> merged = MapMerger.merge(map1, map2, (a, b) -> a + ":" + b);
This example shows how to merge two maps, joining values of duplicate keys via colon.
## License
Apache License, Version 2.0
http://www.apache.org/licenses/LICENSE-2.0